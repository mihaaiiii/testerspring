package com.example.testapplication;

import com.example.testapplication.controller.BookController;
import com.example.testapplication.model.Book;
import com.example.testapplication.service.BookServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest (BookController.class)
public class ControllerBookTest {
    @MockBean
    private BookServices bookServices;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetBook() throws Exception {
        Book book = new Book(1, "Alba ca Zapada", "Vasile Creanga", "1324534564", 1321.90);
        given(bookServices.displayBook(book.getIsbn())).willReturn(book);

mockMvc.perform(get("/book/" + book.getIsbn()))
        .andDo(print())
        .andExpect(status().isOk());

    }

}
