package com.example.testapplication;

import com.example.testapplication.model.Book;
import com.example.testapplication.repository.BookRepository;
import com.example.testapplication.service.BookServices;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith (MockitoExtension.class)
public class ServiceBookTest {

    @Mock
    private BookRepository bookRepository;
    @InjectMocks
    private BookServices bookServices;

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getAllBooks() {
        when(bookRepository.findAll()).thenReturn(new ArrayList<>());
        List<Book> result = bookServices.displayAllBook();
        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void getAllBooks2() {
        when(bookRepository.findAll()).thenReturn(bookList());
        List<Book> result = bookServices.displayAllBook();
        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void addBook() {
        Book book = new Book();
        when(bookRepository.save(book)).thenReturn(book);
        bookServices.createBook(book);
        verify(bookRepository, times(1)).save(book);

    }

    @Test
    public void deleteBook() {
        Book book = new Book(1, "Vasile", "sa", "1234567899", 22.03);
        bookServices.delete(book.getIsbn());
        verify(bookRepository, times(1)).deleteByIsbn(book.getIsbn());

    }

    public List<Book> bookList() {
        List<Book> bookLists = new ArrayList<>();
        Book book1 = new Book();
        Book book2 = new Book();
        Book book3 = new Book();
        bookLists.add(book1);
        bookLists.add(book2);
        bookLists.add(book3);
        return bookLists;
    }


}
