package com.example.testapplication.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookUpdateRequest {

    @NotBlank (message = "Name can't not be blank!")
    private String name;

    @NotBlank (message = "Author name can't not be blank!")
    private String authorName;

    @Positive (message = "The book price must be positive!")
    private Double price;
}
