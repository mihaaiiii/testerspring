package com.example.testapplication.exception;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(String isbn) {
        super("The book whit " + isbn + " not exist!");
    }
}
