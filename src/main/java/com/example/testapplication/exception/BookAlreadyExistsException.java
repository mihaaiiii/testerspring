package com.example.testapplication.exception;

public class BookAlreadyExistsException extends RuntimeException {
    public BookAlreadyExistsException(String isbn) {
        super("The book whit " + isbn + " exist!");
    }
}
