package com.example.testapplication.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name = "books")
public class Book {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    @NotBlank (message = "Name can't not be blank!")
    private String name;
    @Column
    @NotBlank (message = "Author name can't not be blank!")
    private String authorName;
    @Column (unique = true)
    @Pattern (regexp = "^([0-9]{10})", message = "The isbn format must be valid!")
    private String isbn;
    @Column
    @Positive (message = "The book price must be positive!")
    private Double price;


}
