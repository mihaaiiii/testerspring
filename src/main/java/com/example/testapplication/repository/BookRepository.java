package com.example.testapplication.repository;

import com.example.testapplication.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Optional<Book> findByIsbn(String isbn);
    @Transactional
    @Modifying
    @Query("DELETE FROM Book WHERE isbn = :isbn")
    void deleteByIsbn(String isbn);
}
