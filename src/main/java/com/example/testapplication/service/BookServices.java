package com.example.testapplication.service;

import com.example.testapplication.dto.BookUpdateRequest;
import com.example.testapplication.exception.BookAlreadyExistsException;
import com.example.testapplication.exception.BookNotFoundException;
import com.example.testapplication.model.Book;
import com.example.testapplication.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServices {
    private final BookRepository bookRepository;

    public BookServices(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    //Create
    public void createBook(Book book) {
        Optional<Book> optionalBook = bookRepository.findByIsbn(book.getIsbn());
        if (!optionalBook.isPresent()) {
            bookRepository.save(book);
        } else {
            throw new BookAlreadyExistsException(book.getIsbn());
        }
    }

    // READ
    public Book displayBook(String isbn) {
        //Sa returneze book
        return bookRepository.findByIsbn(isbn).orElseThrow(() -> new BookNotFoundException(isbn));
    }

    //READ ALL
    public List<Book> displayAllBook() {

        return bookRepository.findAll();
    }

    //UPDATE
    public void update(String isbn, BookUpdateRequest bookUpdateRequest) {
        Optional<Book> bookToBeUpdated = bookRepository.findByIsbn(isbn);
        if (bookToBeUpdated.isPresent()) {
            Book updateBook = new Book();
            updateBook.setId(bookToBeUpdated.get().getId());
            updateBook.setIsbn(bookToBeUpdated.get().getIsbn());
            updateBook.setName(bookUpdateRequest.getName());
            updateBook.setAuthorName(bookUpdateRequest.getAuthorName());
            updateBook.setPrice(bookUpdateRequest.getPrice());
            bookRepository.save(updateBook);
        } else {
            throw new BookNotFoundException(isbn);
        }

    }

    //DELETE
    public void delete(String isbn) {

        bookRepository.deleteByIsbn(isbn);
    }

    //CRUD


}
