package com.example.testapplication.controller;

import com.example.testapplication.dto.BookUpdateRequest;
import com.example.testapplication.model.Book;
import com.example.testapplication.service.BookServices;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/book")
public class BookController {

    private final BookServices bookServices;

    public BookController(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    @GetMapping ("/{isbn}")
    @ResponseStatus (HttpStatus.OK)
    public Book getBook(@PathVariable String isbn) {
        return bookServices.displayBook(isbn);
    }

    @GetMapping
    @ResponseStatus (HttpStatus.OK)
    public List<Book> getAllBook() {
        return bookServices.displayAllBook();
    }

    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    public void postBook(@Valid @RequestBody Book book) {
        bookServices.createBook(book);
    }

    @DeleteMapping ("/{isbn}")
    @ResponseStatus (HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable String isbn) {
        bookServices.delete(isbn);
    }

    @PutMapping("/{isbn}")
    @ResponseStatus (HttpStatus.ACCEPTED)
    public void updateBook(@PathVariable String isbn, @Valid @RequestBody BookUpdateRequest bookUpdateRequest) {
        bookServices.update(isbn, bookUpdateRequest);
    }

    // PUT  GET POST DELETE

}
